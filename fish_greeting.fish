function fish_greeting
    if test -z "$TMUX"
        echo (set_color red)"Not on tmux."(set_color normal)
    else
        tmux source "$HOME/.local/share/omf/themes/min/min.tmuxtheme"
    end
end
