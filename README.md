[![GPL License](https://img.shields.io/badge/license-GPL-blue.svg?longCache=true&style=flat-square)](/LICENSE)
[![Fish Shell Version](https://img.shields.io/badge/fish-v2.7.1-blue.svg?style=flat-square)](https://fishshell.com)
[![Oh My Fish Framework](https://img.shields.io/badge/Oh%20My%20Fish-Framework-blue.svg?style=flat-square)](https://www.github.com/oh-my-fish/oh-my-fish)

# min

> A theme for [Oh My Fish](https://www.github.com/oh-my-fish/oh-my-fish)

A minimal prompt for the fish shell, to be used with [tmux](https://tmux.github.io).

![screenshot](screenshot.png)

## Install

```fish
omf repositories add https://gitlab.com/argonautica/argonautica 
omf install min
```
